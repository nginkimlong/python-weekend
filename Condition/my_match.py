# Match case can work only python version >= 3.10
x = int(input("Please input value: "))
match(x): 
    case 1: 
        print("One")
    case 2: 
        print("Two")
    case 3: 
        print("Three")
    case 4: 
        print("Four")
    case 5: 
        print("Five")
    case 6: 
        print("Six")
    case _:
        print("Else case")