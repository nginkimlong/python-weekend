lst = ("student 1", "student 2", "student 3", "student 4")
stu2 = lst[1]
print(stu2)
# first method
for i in lst:
    print(i)
print(len(lst))
# second method
for i in range(0, len(lst)):
    print(lst[i])
# Example of for loop
for i in range(1, 11):
    print(i)
lst.sort()

print(lst)
