from sys import stdout
stu = {
    "A": [90, 80, 90, 100],
    "B": [90, 50, 70, 80], 
    "C": [100, 80, 70, 65]
}
stu["D"] = [30, 90, 100, 80]
print(stu.items())

for k, v in stu.items():
    print(v)
