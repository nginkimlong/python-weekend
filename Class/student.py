class Student: 
    # Abstract class
    def __init__(self, id, name, sex, age, salary):
        self.id = id 
        self.name = name 
        self.sex = sex 
        self.age = age 
        self.salary = salary

    def show_student(self): 
        print(self.id, self.name, self.sex, self.age, self.salary)
obj = Student('001', 'Sokny', 'Female', '19', 2000)
obj.show_student()

class Result(Student): 
    def __init__(self, id, name, sex, age, salary, score):
        super().__init__(id, name, sex, age, salary)
        self.score  = score
    def show_result(self): 
        print("Result: ", self.score)

obj2 = Result("001", "Sokny", "F", 18, 200, [99, 80, 90])
obj2.show_student()
obj2.show_result()
        