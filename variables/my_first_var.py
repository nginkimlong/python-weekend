name = " ACLEDA INSTITUTE OF BUSINESS " #string
print("========= ABOUT STRING ==========")
str = name.title() #convert to title style
print(str)
str = name.lower() #convert all letter to small case
print(str)
str = name.strip() #remove space between left and right text
print(str)
str = name[0:10]
print(str)
print(chr(65))
if 'A' > 'a':
    print("Correct!")
else:
    print("Incorrect!")



room = '001'
campus = """New campus"""
room_size = 40
income = 3000.45
status = True
