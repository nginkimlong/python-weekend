# for i in range(1, 10): 
#     print(i)

arr_student = ['Student A', 'Student B', 'Student C']
# First method 
for i in arr_student:
    print(i)
# Second method 
for i in range(0, len(arr_student)):
    print(arr_student[i])